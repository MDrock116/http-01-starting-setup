import axios from "axios";


const instance = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com/'
});

instance.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';

//axios.interceptors.request... //can override interceptors here as well

export default instance;