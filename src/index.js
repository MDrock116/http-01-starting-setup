import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from "axios";

axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com/';  //default for all posts
axios.defaults.headers.common['Authorization'] = 'AUTH TOKEN'; //just an example if you where actually using Auth Token
axios.defaults.headers.post['Content-Type'] = 'application/json'; //not necessary, this is default value, but just to show Content-Type can be changed for posts here

axios.interceptors.request.use(request => { //used just if making the request fails (like no internet connection)
    console.log(request)
    //Edit request config to add your own details
    return request;
}, error => {
    console.log(error);  //we could log to a global server
    return Promise.reject(error); //forward to request in component where we can handle again w/ catch method
})

axios.interceptors.response.use(response => { //used just if making the request fails (like no internet connection)
    console.log(response)
    //Edit response config to add your own details
    return response;  //
}, error => {
    console.log(error);  //we could log to a global server
    return Promise.reject(error); //forward to response in component where we can handle again w/ catch method
})

ReactDOM.render( <App />, document.getElementById( 'root' ) );
registerServiceWorker();
