import React, { Component } from 'react';

import './FullPost.css';
import axios from "axios";

class FullPost extends Component {
    state = {
        loadedPost: null
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.id){
            if(!this.state.loadedPost || this.state.loadedPost.id !== this.props.id){ //to prevent infinite loop (i.e. componentDidMount being called everytime we change state below) check if we don't have a loaded post or if we do have one, are the id's different
                axios.get('/posts/' + this.props.id)
                    .then(r => {
                        //console.log(r)
                        this.setState({loadedPost: r.data})
                    })
            }
        }
    }

    deletePostHandler = () => {
        axios.delete('/posts/' + this.props.id)
            .then(r => {
                console.log(r);
            });
    }

    render () {
        let post = <p style={{textAlign: 'center'}}>Please select a Post!</p>;
        if(this.props.id){
            post = <p style={{textAlign: 'center'}}>Loading...!</p>;
        }
        if(this.state.loadedPost){
            post = (
                <div className="FullPost">
                    <h1>{this.state.loadedPost.title}</h1>
                    <p>{this.state.loadedPost.body}</p>
                    <div className="Edit">
                        <button onClick={this.deletePostHandler} className="Delete">Delete</button>
                    </div>
                </div>

            );
        }

        return post;
    }
}

export default FullPost;